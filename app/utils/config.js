const config = {
  firebaseConfig: {
    apiKey: 'AIzaSyB2cmeBdB2mgrrsH6P9ZmW1JeMP0VBl4MI',
    authDomain: 'datank-c5afe.firebaseapp.com',
    databaseURL: 'https://datank-c5afe.firebaseio.com',
    projectId: 'datank-c5afe',
    storageBucket: '',
    messagingSenderId: '970018238866',
    appId: '1:970018238866:web:33b92568688de9f571514b',
  },
  API_BASE:
    process.env.NODE_ENV === 'development'
      ? 'http://localhost:8080/api'
      : 'https://datank.appspot.com/api',
};

export default config;
