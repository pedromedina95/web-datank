import styled from 'styled-components';

export const SpaceBetween = styled.div`
  display: flex;
  justify-content: space-between;
  ${props => (props.alignVertical ? 'align-items: center;' : '')}
`;

export const AlignVertical = styled.div`
  display: flex;
  align-items: center;
`;

export const LabelButton = styled.button`
  background: transparent;
  border: none;
  outline: none;
  font-size: 14px;
  color: ${props => props.color || '#56575c'};
  cursor: pointer;
  transition: 0.25s opacity;

  &:hover {
    opacity: 0.7;
  }
`;

export const Button = styled.button`
  background: ${props => props.background || '#56575c'};
  border: none;
  outline: none;
  font-size: 14px;
  letter-spacing: 0.28px;
  padding: ${props => props.padding || '12px 32px'};
  border-radius: 8px;
  color: ${props => props.color || 'white'};
  cursor: pointer;
  transition: 0.25s opacity;
  white-space: nowrap;

  &:hover {
    opacity: 0.7;
  }

  &:disabled {
    background: gray;
    opacity: 1 !important;
  }
`;

export const FloatingButton = styled.button`
  background: ${props => props.background || '#56575c'};
  border: none;
  outline: none;
  font-size: 14px;
  letter-spacing: 0.28px;
  padding: ${props => props.padding || '12px 32px'};
  border-radius: 8px;
  color: ${props => props.color || 'white'};
  cursor: pointer;
  transition: 0.25s opacity;
  position: fixed;
  bottom: 16px;
  right: 16px;
  box-shadow: 0px 3px 5px -1px rgba(0, 0, 0, 0.2),
    0px 6px 10px 0px rgba(0, 0, 0, 0.14), 0px 1px 18px 0px rgba(0, 0, 0, 0.12);

  &:hover {
    opacity: 0.7;
  }
`;

export const TabButton = styled.button`
  background: ${({ selected }) => (selected ? '#ebebeb' : '#DFE3E8')};
  color: ${({ selected }) => (selected ? '#000000' : '#454F5B')};
  border: none;
  outline: none;
  font-size: 14px;
  letter-spacing: 0.28px;
  padding: 12px 24px;
  border-radius: 12px;
  cursor: pointer;
  transition: 0.25s all;
  margin-right: ${({ noMargin }) => (noMargin ? 0 : 16)}px;
  margin-bottom: ${({ noMargin }) => (noMargin ? 0 : 16)}px;

  &:hover {
    background: #ebebeb;
    color: #000000;
  }
`;

export const FloatContentRight = styled.div`
  display: flex;
  justify-content: flex-end;
`;

export const O = styled.div`
  padding: 24px 0;
  text-align: center;
`;
