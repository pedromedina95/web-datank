/**
 *
 * GoogleLogin
 *
 */

import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import firebase from 'firebase';
import 'firebase/auth';
import 'firebase/database';
import Button from '@material-ui/core/Button';
import config from 'utils/config';
import PropTypes from 'prop-types';
import superagent from 'superagent';
import { styles } from './styledComponents';
import messages from './messages';

function GoogleLogin(props) {
  const googleProvider = new firebase.auth.GoogleAuthProvider();

  firebase.auth().useDeviceLanguage();
  function tryLogin() {
    firebase
      .auth()
      .signInWithPopup(googleProvider)
      .then(result => {
        // const token = result.credential.accessToken;
        const { user } = result;
        superagent
          .post(`${config.API_BASE}/new-user`)
          .send({
            email: user.email,
            password: '12341234',
            type: 'normal',
            name: user.displayName,
          })
          .then(response => {
            // localStorage.setItem('token', token);
            localStorage.setItem('user', JSON.stringify(response.body));
            props.history.push('/eventos');
          });
      })
      .catch(() => {
        console.log('errorMessage', errorMessage);
      });
  }
  return (
    <Button
      className={props.classes.gButton}
      variant="outlined"
      style={styles.label}
      onClick={tryLogin}
    >
      <img
        width="32"
        height="32"
        className={props.classes.gLogo}
        src="https://storage.googleapis.com/primavera-condominos/backoffice/assets/google-icon.png"
        alt="Google Icon"
      />
      {messages.loginMessage}
    </Button>
  );
}

GoogleLogin.propTypes = {
  classes: PropTypes.object.isRequired,
  // dispatch: PropTypes.func.isRequired,
  history: PropTypes.object,
};

export default withStyles(styles)(GoogleLogin);
