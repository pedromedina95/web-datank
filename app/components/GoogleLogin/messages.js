import { defineMessages } from 'react-intl';

export const scope = 'app.components.LoginWithGoogleButton';

export default defineMessages({
  loginMessage: 'INGRESAR CON GOOGLE',
});
