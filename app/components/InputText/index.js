/**
 *
 * Input
 *
 */

import React, { memo } from 'react';
import PropTypes from 'prop-types';
import TextField from '@material-ui/core/TextField';
import { withStyles, createMuiTheme } from '@material-ui/core/styles';
import { ThemeProvider } from '@material-ui/styles';
import { grey } from '@material-ui/core/colors';
import classNames from 'classnames';

const theme = createMuiTheme({
  palette: {
    primary: grey,
  },
  typography: {
    fontFamily: ['product-sans', '"Helvetica Neue"', 'Helvetica', 'Arial'],
  },
});

const styles = () => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    flexBasis: 200,
    width: '100%',
  },
  margin: {
    marginBottom: 32,
  },
});

function Input(props) {
  const { classes } = props;
  return (
    <ThemeProvider theme={theme}>
      <TextField
        {...props}
        className={classNames(classes.margin, classes.root)}
        variant="filled"
        InputProps={{
          ...props.InputProps,
        }}
      />
    </ThemeProvider>
  );
}

Input.propTypes = {
  classes: PropTypes.object,
  InputProps: PropTypes.object,
};

export default withStyles(styles)(memo(Input));
