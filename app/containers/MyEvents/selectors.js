import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the myEvents state domain
 */

const selectMyEventsDomain = state => state.myEvents || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by MyEvents
 */

const makeSelectMyEvents = () =>
  createSelector(
    selectMyEventsDomain,
    substate => substate,
  );

export default makeSelectMyEvents;
export { selectMyEventsDomain };
