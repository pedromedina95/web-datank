/**
 *
 * MyEvents
 *
 */

import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import makeSelectMyEvents from './selectors';
import reducer from './reducer';
import saga from './saga';

export function MyEvents() {
  useInjectReducer({ key: 'myEvents', reducer });
  useInjectSaga({ key: 'myEvents', saga });

  return (
    <div>
      <Helmet>
        <title>MyEvents</title>
        <meta name="description" content="Description of MyEvents" />
      </Helmet>
    </div>
  );
}

MyEvents.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  myEvents: makeSelectMyEvents(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(MyEvents);
