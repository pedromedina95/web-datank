import styled from 'styled-components';

export const Detail = styled.div`
  margin: 8px 0;
  & span {
    margin-left: 16px;
  }
`;
