/**
 *
 * Events
 *
 */

import React, { memo, useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import CalendarIcon from '@material-ui/icons/CalendarToday';
import WatchIcon from '@material-ui/icons/WatchLater';
import MapIcon from '@material-ui/icons/Map';
import Tooltip from '@material-ui/core/Tooltip';

import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/styles';

import { Button, LabelButton } from 'utils/globalStyledComponents';
import superagent from 'superagent';
import config from 'utils/config';
import { get } from 'lodash';

import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import makeSelectEvents from './selectors';
import reducer from './reducer';
import saga from './saga';
import { Detail } from './styledComponents';

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing(3),
    overflowX: 'auto',
  },
  table: {
    minWidth: 650,
  },
}));

export function Events() {
  useInjectReducer({ key: 'events', reducer });
  useInjectSaga({ key: 'events', saga });
  const classes = useStyles();
  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));
  const [dialogOpen, setDialogOpen] = useState(false);
  const [rows, setRows] = useState([]);
  const [assistants, setAssistants] = useState([]);
  const [selectedItem, setSelectedItem] = useState(null);

  const user = JSON.parse(localStorage.getItem('user'));

  async function fetchData() {
    superagent.get(`${config.API_BASE}/events`).then(response => {
      setRows(response.body);
    });
    superagent
      .get(`${config.API_BASE}/assistants/${user.id}`)
      .then(response => {
        setAssistants(response.body);
      });
  }

  useEffect(() => {
    fetchData();
  }, []);

  const handleCloseDialog = () => {
    setDialogOpen(false);
  };
  const handleOnAsist = item => {
    setSelectedItem(item);
    setDialogOpen(true);
  };

  const handleConfirmAsist = () => {
    superagent
      .post(`${config.API_BASE}/assistants`)
      .send({
        user: user.id,
        event: selectedItem.id,
      })
      .then(() => {
        fetchData();
        handleCloseDialog();
      });
  };
  return (
    <div>
      <Helmet>
        <title>Eventos</title>
        <meta name="description" content="Description of Events" />
      </Helmet>
      <Paper className={classes.root}>
        <Table className={classes.table}>
          <TableHead>
            <TableRow>
              <TableCell>Tema</TableCell>
              <TableCell>Fecha</TableCell>
              <TableCell>Horario</TableCell>
              <TableCell>Dirección</TableCell>
              <TableCell />
            </TableRow>
          </TableHead>
          <TableBody>
            {rows.map(row => (
              <TableRow key={row.id}>
                <TableCell component="th" scope="row">
                  {row.topic}
                </TableCell>
                <TableCell>{row.date}</TableCell>
                <TableCell>{`${row.scheduleStart} - ${
                  row.scheduleEnd
                }`}</TableCell>
                <TableCell>{row.address}</TableCell>
                <TableCell align="right">
                  {assistants.find(i => i.event === row.id) ? (
                    <span>Confirmado</span>
                  ) : (
                    <Button
                      onClick={() => handleOnAsist(row)}
                      padding="8px 16px"
                      disabled={!!assistants.find(i => i.event === row.id)}
                    >
                      Asistiré
                    </Button>
                  )}
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </Paper>
      <Dialog
        open={dialogOpen}
        onClose={handleCloseDialog}
        aria-labelledby="form-dialog-title"
        fullScreen={fullScreen}
      >
        <DialogTitle id="form-dialog-title">Detalles del evento</DialogTitle>
        <DialogContent>
          <DialogContentText>
            {get(selectedItem, 'diary', '')}
          </DialogContentText>
          <Detail>
            <CalendarIcon />
            <span>{get(selectedItem, 'date', '')}</span>
          </Detail>
          <Detail>
            <WatchIcon />
            <span>{`${get(selectedItem, 'scheduleStart', '')} - ${get(
              selectedItem,
              'scheduleEnd',
              '',
            )}`}</span>
          </Detail>
          <Detail>
            <MapIcon />
            <span>{get(selectedItem, 'address', '')}</span>
          </Detail>
          <Detail style={{ marginTop: 32 }}>
            <b>No te pierdas las charlas</b>
          </Detail>
          <ul>
            {JSON.parse(get(selectedItem, 'talks', '[]')).map(item => (
              <Tooltip
                title={
                  <div style={{ fontSize: 12 }}>
                    <div>{`${item.aboutYou}. ${item.aboutTalk}.`}</div>
                    {item.twitter && (
                      <div>{`Sígueme en twitter ${item.twitter}`}</div>
                    )}
                    {item.facebook && (
                      <div>{`Sígueme en facebook ${item.facebook}`}</div>
                    )}
                    {item.linkedin && (
                      <div>{`Sígueme en linkedin ${item.linkedin}`}</div>
                    )}
                  </div>
                }
              >
                <li style={{ width: 'fit-content' }}>
                  <Detail>{`${item.title} (${item.userName})`}</Detail>
                </li>
              </Tooltip>
            ))}
          </ul>
        </DialogContent>
        <DialogActions>
          <LabelButton style={{ marginRight: 24 }} onClick={handleCloseDialog}>
            Cancelar
          </LabelButton>
          <Button onClick={handleConfirmAsist}>Confirmar</Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}

Events.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  events: makeSelectEvents(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(Events);
