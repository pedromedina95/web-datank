import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the talkRequest state domain
 */

const selectTalkRequestDomain = state => state.talkRequest || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by TalkRequest
 */

const makeSelectTalkRequest = () =>
  createSelector(
    selectTalkRequestDomain,
    substate => substate,
  );

export default makeSelectTalkRequest;
export { selectTalkRequestDomain };
