import styled from 'styled-components';

export const Form = styled.div`
  background-color: #ffffff;
  box-shadow: 0 1px 5px 0 rgba(0, 0, 0, 0.1);
  padding: 32px;
  border-radius: 4px;
  max-width: 636px;

  @media (max-width: 768px) {
    padding: 24px 16px;
  }
`;
