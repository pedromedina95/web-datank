/**
 *
 * TalkRequest
 *
 */

import React, { memo, useState } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import Input from 'components/InputText';
import { Button, FloatContentRight } from 'utils/globalStyledComponents';

import { Formik, Field } from 'formik';
import * as Yup from 'yup';
import superagent from 'superagent';
import config from 'utils/config';
import Snackbar from '@material-ui/core/Snackbar';

import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import makeSelectTalkRequest from './selectors';
import reducer from './reducer';
import saga from './saga';
import { Form } from './styledComponents';

const user = JSON.parse(localStorage.getItem('user'));

export function TalkRequest() {
  useInjectReducer({ key: 'talkRequest', reducer });
  useInjectSaga({ key: 'talkRequest', saga });
  const [snackbarOpen, setSnackbarOpen] = useState(false);

  const validationSchema = Yup.object({
    // user: Yup.string().required('Campo requerido'),
    userName: Yup.string().required('Campo requerido'),
    email: Yup.string()
      .email('Ingrese un correo válido')
      .required('Campo requerido'),
    title: Yup.string().required('Campo requerido'),
    aboutTalk: Yup.string().required('Campo requerido'),
    aboutYou: Yup.string().required('Campo requerido'),
    twitter: Yup.string(),
    linkedin: Yup.string(),
    facebook: Yup.string(),
  });

  const defaultValues = {
    // user: '',
    userName: '',
    email: '',
    title: '',
    aboutTalk: '',
    aboutYou: '',
    twitter: '',
    linkedin: '',
    facebook: '',
  };

  const handleCreate = values => {
    superagent
      .post(`${config.API_BASE}/talk`)
      .send({
        ...values,
        user: user.id,
      })
      .then(() => {
        setSnackbarOpen(true);
      });
  };

  return (
    <div>
      <Helmet>
        <title>Quiero dar una charla</title>
        <meta name="description" content="Description of TalkRequest" />
      </Helmet>
      <Formik
        onSubmit={(values, { setSubmitting }) => {
          setSubmitting(true);
          handleCreate(values);
        }}
        initialValues={defaultValues}
        validationSchema={validationSchema}
        render={({
          values,
          disabled,
          errors,
          touched,
          handleSubmit,
          isValid,
        }) => (
          <Form>
            <Field
              defaultValue={values.userName}
              name="userName"
              render={({ field }) => (
                <Input
                  {...field}
                  label="Nombre de contacto"
                  helperText={touched.userName ? errors.userName : ''}
                  error={touched.userName && Boolean(errors.userName)}
                />
              )}
            />
            <Field
              defaultValue={values.email}
              name="email"
              render={({ field }) => (
                <Input
                  {...field}
                  label="Correo electrónico"
                  helperText={touched.email ? errors.email : ''}
                  error={touched.email && Boolean(errors.email)}
                />
              )}
            />
            <Field
              defaultValue={values.title}
              name="title"
              render={({ field }) => (
                <Input
                  {...field}
                  label="Título de charla"
                  helperText={touched.title ? errors.title : ''}
                  error={touched.title && Boolean(errors.title)}
                />
              )}
            />
            <Field
              defaultValue={values.aboutTalk}
              name="aboutTalk"
              render={({ field }) => (
                <Input
                  {...field}
                  label="Pláticanos un poco sobre la charla"
                  helperText={touched.aboutTalk ? errors.aboutTalk : ''}
                  error={touched.aboutTalk && Boolean(errors.aboutTalk)}
                  multiline
                  rows="4"
                />
              )}
            />
            <Field
              defaultValue={values.aboutYou}
              name="aboutYou"
              render={({ field }) => (
                <Input
                  {...field}
                  label="Pláticanos un poco sobre tí"
                  helperText={touched.aboutYou ? errors.aboutYou : ''}
                  error={touched.aboutYou && Boolean(errors.aboutYou)}
                  multiline
                  rows="4"
                />
              )}
            />
            <Field
              defaultValue={values.twitter}
              name="twitter"
              render={({ field }) => (
                <Input
                  {...field}
                  label="¿Tienes twitter?"
                  helperText={touched.twitter ? errors.twitter : ''}
                  error={touched.twitter && Boolean(errors.twitter)}
                />
              )}
            />
            <Field
              defaultValue={values.linkedin}
              name="linkedin"
              render={({ field }) => (
                <Input
                  {...field}
                  label="¿Tienes linkedin?"
                  helperText={touched.linkedin ? errors.linkedin : ''}
                  error={touched.linkedin && Boolean(errors.linkedin)}
                />
              )}
            />
            <Field
              defaultValue={values.facebook}
              name="facebook"
              render={({ field }) => (
                <Input
                  {...field}
                  label="¿Tienes facebook?"
                  helperText={touched.facebook ? errors.facebook : ''}
                  error={touched.facebook && Boolean(errors.facebook)}
                />
              )}
            />
            <FloatContentRight>
              <Button onClick={handleSubmit} disabled={disabled || !isValid}>
                Enviar
              </Button>
            </FloatContentRight>
          </Form>
        )}
      />
      <Snackbar
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'left',
        }}
        open={snackbarOpen}
        autoHideDuration={6000}
        onClose={() => setSnackbarOpen(false)}
        ContentProps={{
          'aria-describedby': 'message-id',
        }}
        message={<span id="message-id">¡Propuesta enviada con éxito!</span>}
      />
    </div>
  );
}

TalkRequest.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  talkRequest: makeSelectTalkRequest(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(TalkRequest);
