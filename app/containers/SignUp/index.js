/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 *
 */

import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import Input from 'components/InputText';
import firebase from 'firebase';
import 'firebase/auth';
import 'firebase/database';
import superagent from 'superagent';
import config from 'utils/config';

import { Formik, Field } from 'formik';
import * as Yup from 'yup';

import {
  SpaceBetween,
  LabelButton,
  Button,
} from 'utils/globalStyledComponents';
import {
  MainContainer,
  FormSection,
  Logo,
  H1,
  P,
  Form,
} from './styledComponents';

function equalTo(ref, msg) {
  return Yup.mixed().test({
    name: 'equalTo',
    exclusive: false,
    message: msg || '${path} must be the same as ${reference}',// eslint-disable-line
    params: {
      reference: ref.path,
    },
    test(value) {
      return value === this.resolve(ref);
    },
  });
}
Yup.addMethod(Yup.string, 'equalTo', equalTo);

export default function HomePage({ history }) {
  useEffect(() => {
    const user = JSON.parse(localStorage.getItem('user'));
    if (user && user.type === 'normal') {
      history.push('/eventos');
      return;
    }
    if (user) history.push('/catalogo-eventos');
  }, []);

  const handleLogin = () => {
    history.push('/inicio-sesion');
  };

  const tryLogin = values => {
    const { name, email, password } = values;
    firebase
      .auth()
      .createUserWithEmailAndPassword(email, password)
      .then(() => {
        superagent
          .post(`${config.API_BASE}/new-user`)
          .send({
            email,
            password,
            type: 'normal',
            name,
          })
          .then(response => {
            localStorage.setItem('user', JSON.stringify(response.body));
            history.push('/eventos');
          });
      })
      .catch(err => {
        console.log('err', err);
      });
  };

  const validationSchema = Yup.object({
    name: Yup.string('Ingrese su nombre').required('Campo requerido'),
    email: Yup.string('Ingrese su correo')
      .email('Ingrese un correo válido')
      .required('Campo requerido'),
    password: Yup.string('Ingrese una contraseña')
      .min(6, 'Al menos 6 caracteres')
      .required('Campo requerido'),
    passwordConfirm: Yup.string('Ingrese de nuevo la contraseña')
      .equalTo(Yup.ref('password'), 'Las contraseñas no coinciden')
      .required('Campo requerido'),
  });

  const defaultValues = {
    name: '',
    email: '',
    password: '',
    passwordConfirm: '',
  };

  return (
    <MainContainer>
      <FormSection>
        <Logo src="https://pbs.twimg.com/profile_images/902297422226898944/1zYIFQiS_400x400.jpg" />
        <H1>Bienvenido a Datank</H1>
        <P>Ingrese la siguiente información para registrarse</P>
        <Formik
          onSubmit={(values, { setSubmitting }) => {
            tryLogin(values);
            setSubmitting(true);
          }}
          initialValues={defaultValues}
          validationSchema={validationSchema}
          render={({
            values,
            disabled,
            errors,
            touched,
            handleSubmit,
            isValid,
          }) => (
            <Form noValidate autoComplete="off" onSubmit={handleSubmit}>
              <Field
                defaultValue={values.name}
                name="name"
                render={({ field }) => (
                  <Input
                    {...field}
                    label="Nombre completo"
                    helperText={touched.name ? errors.name : ''}
                    error={touched.name && Boolean(errors.name)}
                  />
                )}
              />
              <Field
                defaultValue={values.email}
                name="email"
                render={({ field }) => (
                  <Input
                    {...field}
                    label="Correo"
                    helperText={touched.email ? errors.email : ''}
                    error={touched.email && Boolean(errors.email)}
                  />
                )}
              />
              <Field
                defaultValue={values.password}
                name="password"
                render={({ field }) => (
                  <Input
                    {...field}
                    label="Contraseña"
                    helperText={touched.password ? errors.password : ''}
                    error={touched.password && Boolean(errors.password)}
                    type="password"
                  />
                )}
              />
              <Field
                defaultValue={values.passwordConfirm}
                name="passwordConfirm"
                render={({ field }) => (
                  <Input
                    {...field}
                    label="Confirmar contraseña"
                    helperText={
                      touched.passwordConfirm ? errors.passwordConfirm : ''
                    }
                    error={
                      touched.passwordConfirm && Boolean(errors.passwordConfirm)
                    }
                    type="password"
                  />
                )}
              />
              <SpaceBetween>
                <LabelButton onClick={handleLogin}>
                  Ya tengo una cuenta
                </LabelButton>
                <Button actionType="submit" disabled={disabled || !isValid}>
                  Registrar
                </Button>
              </SpaceBetween>
            </Form>
          )}
        />
      </FormSection>
    </MainContainer>
  );
}

HomePage.propTypes = {
  history: PropTypes.object.isRequired,
};
