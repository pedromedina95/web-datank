/**
 *
 * EventsAdmin
 *
 */

import React, { memo, useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/styles';
import Select from 'components/Select';

import { Formik, Field } from 'formik';
import * as Yup from 'yup';
import superagent from 'superagent';
import config from 'utils/config';
import moment from 'moment';

import DateFnsUtils from '@date-io/date-fns';
import {
  MuiPickersUtilsProvider,
  KeyboardTimePicker,
  KeyboardDatePicker,
} from '@material-ui/pickers';

import Input from 'components/InputText';
import { get } from 'lodash';

import {
  Button,
  LabelButton,
  FloatingButton,
} from 'utils/globalStyledComponents';

import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import makeSelectEventsAdmin from './selectors';
import reducer from './reducer';
import saga from './saga';

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing(3),
    overflowX: 'auto',
  },
  table: {
    minWidth: 650,
  },
}));

const iconStyle = {
  color: '#56575c',
  cursor: 'pointer',
  marginLeft: 16,
};

export function EventsAdmin() {
  useInjectReducer({ key: 'eventsAdmin', reducer });
  useInjectSaga({ key: 'eventsAdmin', saga });
  const classes = useStyles();
  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));
  const [deleteDialogOpen, setDeleteDialogOpen] = useState(false);
  const [createDialogOpen, setCreateDialogOpen] = useState(false);
  const [selectedDate, setSelectedDate] = useState(new Date());
  const [startHour, setStartHour] = useState(new Date());
  const [finishHour, setFinishHour] = useState(new Date());
  const [rows, setRows] = useState([]);
  const [talksList, setTalks] = useState([]);
  const [selectedTalks, setSelectedTalks] = useState([]);
  const [itemToDelete, setItemToDelete] = useState(null);
  const [itemToEdit, setItemToEdit] = useState(null);

  async function fetchData() {
    superagent.get(`${config.API_BASE}/events`).then(response => {
      setRows(response.body);
    });
    superagent.get(`${config.API_BASE}/talks`).then(response => {
      setTalks(
        response.body.map(talk => ({
          ...talk,
          value: talk.id,
          label: talk.title,
        })),
      );
    });
  }

  useEffect(() => {
    fetchData();
  }, []);

  const validationSchema = Yup.object({
    topic: Yup.string().required('Campo requerido'),
    address: Yup.string().required('Campo requerido'),
    diary: Yup.string().required('Campo requerido'),
    talks: Yup.string()
      .typeError('Campo requerido')
      .required('Campo requerido'),
  });

  const defaultValues = {
    topic: get(itemToEdit, 'topic', ''),
    address: get(itemToEdit, 'address', ''),
    diary: get(itemToEdit, 'diary', ''),
    talks: get(itemToEdit, 'talks', '[]'),
  };

  const handleCloseDeleteDialog = () => {
    setDeleteDialogOpen(false);
  };
  const handleOnDelete = item => {
    setItemToDelete(item);
    setDeleteDialogOpen(true);
  };
  const handleCloseUpdateDialog = () => {
    setItemToEdit(null);
    setCreateDialogOpen(false);
    selectedTalks([]);
  };
  const handleOnEdit = item => {
    setItemToEdit(item);
    setCreateDialogOpen(true);
  };
  const handleOnCreate = () => {
    setCreateDialogOpen(true);
  };
  const handleCreate = values => {
    const { topic, address, diary, talks } = values;
    const body = {
      topic,
      address,
      diary,
      date: selectedDate,
      scheduleStart: moment(startHour).format('LT'),
      scheduleEnd: moment(finishHour).format('LT'),
      talks: talks || '[]',
    };
    if (itemToEdit) {
      superagent
        .patch(`${config.API_BASE}/event/${itemToEdit.id}`)
        .send(body)
        .then(() => {
          fetchData();
          setItemToEdit(null);
          setCreateDialogOpen(false);
          selectedTalks([]);
        });
    } else {
      superagent
        .post(`${config.API_BASE}/new-event`)
        .send(body)
        .then(() => {
          fetchData();
          setItemToEdit(null);
          setCreateDialogOpen(false);
          selectedTalks([]);
        });
    }
  };
  const handleDelete = () => {
    superagent
      .patch(`${config.API_BASE}/delete-event/${itemToDelete.id}`)
      .then(() => {
        fetchData();
        setDeleteDialogOpen(false);
      });
  };
  const deleteDialog = (
    <Dialog
      open={deleteDialogOpen}
      onClose={handleCloseDeleteDialog}
      aria-labelledby="form-dialog-title"
      fullScreen={fullScreen}
    >
      <DialogTitle id="form-dialog-title">Eliminar evento</DialogTitle>
      <DialogContent>
        <DialogContentText>
          ¿Está seguro de eliminar? Esta acción no se puede rehacer. Se perderá
          la información permanentemente.
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <LabelButton
          style={{ marginRight: 24 }}
          onClick={handleCloseDeleteDialog}
        >
          Cancelar
        </LabelButton>
        <Button onClick={handleDelete}>Eliminar</Button>
      </DialogActions>
    </Dialog>
  );

  const createDialog = (
    <Dialog
      open={createDialogOpen}
      onClose={handleCloseUpdateDialog}
      aria-labelledby="form-dialog-title"
      fullScreen={fullScreen}
    >
      <DialogTitle id="form-dialog-title">{`${
        itemToEdit ? 'Editar' : 'Crear'
      } evento`}</DialogTitle>
      <Formik
        onSubmit={(values, { setSubmitting }) => {
          setSubmitting(true);
          handleCreate(values);
        }}
        initialValues={defaultValues}
        validationSchema={validationSchema}
        render={({
          values,
          disabled,
          errors,
          touched,
          handleSubmit,
          isValid,
          setFieldTouched,
          setFieldValue,
        }) => (
          <div>
            <DialogContent>
              <DialogContentText>
                Ingrese la siguiente información para crear un nuevo evento
              </DialogContentText>
              <Field
                defaultValue={values.topic}
                name="topic"
                render={({ field }) => (
                  <Input
                    {...field}
                    label="Tema"
                    helperText={touched.topic ? errors.topic : ''}
                    error={touched.topic && Boolean(errors.topic)}
                  />
                )}
              />
              <MuiPickersUtilsProvider utils={DateFnsUtils}>
                <KeyboardDatePicker
                  margin="normal"
                  id="date-picker-dialog"
                  label="Fecha del evento"
                  format="MM/dd/yyyy"
                  value={selectedDate}
                  onChange={date => setSelectedDate(date)}
                  KeyboardButtonProps={{
                    'aria-label': 'change date',
                  }}
                  inputVariant="filled"
                  style={{ width: '100%', marginBottom: 32, marginTop: 0 }}
                />
                <KeyboardTimePicker
                  margin="normal"
                  id="time-picker"
                  label="Hora de inicio"
                  value={startHour}
                  onChange={date => setStartHour(date)}
                  KeyboardButtonProps={{
                    'aria-label': 'change time',
                  }}
                  inputVariant="filled"
                  style={{ width: '100%', marginBottom: 32, marginTop: 0 }}
                />
                <KeyboardTimePicker
                  margin="normal"
                  id="time-picker"
                  label="Hora de finalización"
                  value={finishHour}
                  onChange={date => setFinishHour(date)}
                  KeyboardButtonProps={{
                    'aria-label': 'change time',
                  }}
                  inputVariant="filled"
                  style={{ width: '100%', marginBottom: 32, marginTop: 0 }}
                />
              </MuiPickersUtilsProvider>
              <Field
                defaultValue={values.address}
                name="address"
                render={({ field }) => (
                  <Input
                    {...field}
                    label="Dirección"
                    helperText={touched.address ? errors.address : ''}
                    error={touched.address && Boolean(errors.address)}
                  />
                )}
              />
              <Field
                defaultValue={values.diary}
                name="diary"
                render={({ field }) => (
                  <Input
                    {...field}
                    label="Agenda"
                    helperText={touched.diary ? errors.diary : ''}
                    error={touched.diary && Boolean(errors.diary)}
                    rows={6}
                    multiline
                  />
                )}
              />
              <Select
                defaultValue={JSON.parse(get(itemToEdit, 'talks', '[]'))}
                isMulti
                name="talks"
                options={talksList}
                onChange={newValue => {
                  setSelectedTalks(newValue);
                  setFieldTouched('talks', true);
                  setFieldValue(
                    'talks',
                    newValue && newValue.length
                      ? JSON.stringify(newValue)
                      : null,
                  );
                }}
                placeholder="Charlas"
                menuPlacement="top"
                error={
                  touched.talks && Boolean(errors.talks) ? errors.talks : ''
                }
              />
            </DialogContent>
            <DialogActions>
              <LabelButton
                style={{ marginRight: 24 }}
                onClick={handleCloseUpdateDialog}
              >
                Cancelar
              </LabelButton>
              <Button onClick={handleSubmit} disabled={disabled || !isValid}>
                {itemToEdit ? 'Editar' : 'Crear'}
              </Button>
            </DialogActions>
          </div>
        )}
      />
    </Dialog>
  );
  return (
    <div>
      <Helmet>
        <title>Eventos</title>
        <meta name="description" content="Description of EventsAdmin" />
      </Helmet>
      <Paper className={classes.root}>
        <Table className={classes.table}>
          <TableHead>
            <TableRow>
              <TableCell>Tema</TableCell>
              <TableCell>Fecha</TableCell>
              <TableCell>Horario</TableCell>
              <TableCell>Dirección</TableCell>
              <TableCell />
            </TableRow>
          </TableHead>
          <TableBody>
            {rows.map(row => (
              <TableRow key={row.id}>
                <TableCell component="th" scope="row">
                  {row.topic}
                </TableCell>
                <TableCell>{row.date}</TableCell>
                <TableCell>{`${row.scheduleStart} - ${
                  row.scheduleEnd
                }`}</TableCell>
                <TableCell>{row.address}</TableCell>
                <TableCell align="right">
                  <EditIcon
                    style={iconStyle}
                    onClick={() => handleOnEdit(row)}
                  />
                  <DeleteIcon
                    style={iconStyle}
                    onClick={() => handleOnDelete(row)}
                  />
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
        <FloatingButton onClick={handleOnCreate}>Nuevo</FloatingButton>
      </Paper>
      {deleteDialog}
      {createDialog}
    </div>
  );
}

EventsAdmin.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  eventsAdmin: makeSelectEventsAdmin(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(EventsAdmin);
