import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the eventsAdmin state domain
 */

const selectEventsAdminDomain = state => state.eventsAdmin || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by EventsAdmin
 */

const makeSelectEventsAdmin = () =>
  createSelector(
    selectEventsAdminDomain,
    substate => substate,
  );

export default makeSelectEventsAdmin;
export { selectEventsAdminDomain };
