/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 *
 */

import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import Input from 'components/InputText';
import firebase from 'firebase';
import 'firebase/auth';
import 'firebase/database';
import superagent from 'superagent';
import config from 'utils/config';
import Snackbar from '@material-ui/core/Snackbar';

import {
  SpaceBetween,
  LabelButton,
  Button,
} from 'utils/globalStyledComponents';
import {
  MainContainer,
  FormSection,
  Logo,
  H1,
  P,
  Form,
} from './styledComponents';

export default function HomePage({ history }) {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [snackbarOpen, setSnackbarOpen] = useState(false);
  useEffect(() => {
    const user = JSON.parse(localStorage.getItem('user'));
    if (user && user.type === 'normal') {
      history.push('/eventos');
      return;
    }
    if (user) history.push('/catalogo-eventos');
  }, []);
  const handleLogin = () => {
    tryLogin();
  };
  const handleSignUp = () => {
    history.push('/registro');
  };
  const tryLogin = () => {
    firebase
      .auth()
      .signInWithEmailAndPassword(email, password)
      .then(() => {
        superagent
          .post(`${config.API_BASE}/new-user`)
          .send({
            email,
            password,
            type: 'normal',
            name: '',
          })
          .then(response => {
            localStorage.setItem('user', JSON.stringify(response.body));
            history.push(
              response.body.type === 'admin' ? '/catalogo-eventos' : '/eventos',
            );
          });
      })
      .catch(() => {
        setSnackbarOpen(true);
      });
  };
  return (
    <MainContainer>
      <FormSection>
        <Logo src="https://pbs.twimg.com/profile_images/902297422226898944/1zYIFQiS_400x400.jpg" />
        <H1>Bienvenido a Datank</H1>
        <P>Por favor ingrese sus credenciales para acceder al sistema</P>
        <Form>
          <Input
            label="Correo"
            error={false}
            helperText={false && 'Ingrese un correo válido'}
            onChange={e => setEmail(e.target.value)}
            type="email"
          />
          <Input
            label="Contraseña"
            error={false}
            helperText={false && 'Ingrese una contraseña'}
            type="password"
            onChange={e => setPassword(e.target.value)}
          />
          <SpaceBetween>
            <LabelButton onClick={handleSignUp}>Crear cuenta</LabelButton>
            <Button onClick={handleLogin}>Ingresar</Button>
          </SpaceBetween>
        </Form>
      </FormSection>
      <Snackbar
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'left',
        }}
        open={snackbarOpen}
        autoHideDuration={3000}
        onClose={() => setSnackbarOpen(false)}
        ContentProps={{
          'aria-describedby': 'message-id',
        }}
        message={<span id="message-id">Usuario o contraseña incorrecta</span>}
      />
    </MainContainer>
  );
}

HomePage.propTypes = {
  history: PropTypes.object.isRequired,
};
