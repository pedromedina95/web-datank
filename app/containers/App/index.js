/**
 *
 * App.js
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 *
 */

import React from 'react';
import { Switch, Route } from 'react-router-dom';
import { Helmet } from 'react-helmet';

import { createMuiTheme } from '@material-ui/core/styles';
import { grey } from '@material-ui/core/colors';
import { ThemeProvider } from '@material-ui/styles';

import AppRoute from 'components/AppRoute';
import HomePage from 'containers/HomePage/Loadable';
import NotFoundPage from 'containers/NotFoundPage/Loadable';
import MainLayout from 'containers/MainLayout';
import SignUp from 'containers/SignUp';
import Events from 'containers/Events';
import EventsAdmin from 'containers/EventsAdmin';
import TalkRequest from 'containers/TalkRequest';
import Users from 'containers/Users';
import MyEvents from 'containers/MyEvents';
import DashboardBackoffice from 'containers/DashboardBackoffice';
import firebase from 'firebase';
import app from 'firebase/app';
import config from 'utils/config';

import GlobalStyle from '../../global-styles';

const theme = createMuiTheme({
  palette: {
    primary: grey,
  },
  typography: {
    fontFamily: ['product-sans', '"Helvetica Neue"', 'Helvetica', 'Arial'],
  },
});

export default function App() {
  if (!firebase.apps.length) {
    app.initializeApp(config.firebaseConfig);
  }

  return (
    <div>
      <Helmet titleTemplate="%s - Datank" defaultTitle="Eventos - Datank">
        <meta name="description" content="Eventos de datank" />
      </Helmet>
      <ThemeProvider theme={theme}>
        <Switch>
          <AppRoute
            exact
            path="/"
            layout={MainLayout}
            component={DashboardBackoffice}
          />
          <AppRoute
            exact
            path="/eventos"
            layout={MainLayout}
            component={Events}
          />
          <AppRoute
            exact
            path="/catalogo-eventos"
            layout={MainLayout}
            component={EventsAdmin}
          />
          <AppRoute
            exact
            path="/nueva-charla"
            layout={MainLayout}
            component={TalkRequest}
          />
          <AppRoute
            exact
            path="/usuarios"
            layout={MainLayout}
            component={Users}
          />
          <AppRoute
            exact
            path="/mis-eventos"
            layout={MainLayout}
            component={MyEvents}
          />
          <Route exact path="/inicio-sesion" component={HomePage} />
          <Route exact path="/registro" component={SignUp} />
          <Route component={NotFoundPage} />
        </Switch>
      </ThemeProvider>
      <GlobalStyle />
    </div>
  );
}
