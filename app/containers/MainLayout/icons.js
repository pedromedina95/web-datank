import styled from 'styled-components';
import React from 'react';
import PropTypes from 'prop-types';

export const SidebarContainer = styled.section`
  min-height: 100vh;
  padding-left: 16px;
  padding-top: 26px;
  width: 200px;
  float: left;
`;

export const Subtitle = styled.div`
  color: #b3b3b3;
  font-size: 10px;
  font-weight: 700;
  margin-top: 32px;
  float: left;
  margin-bottom: 16px;
  padding-left: 8px;
`;

export const SidebarItem = styled.span`
  width: 100%;
  padding: 10px 16px;
  align-items: center;
  margin-bottom: 12px;
  border-radius: 12px;
  transition: background 0.25s;
  cursor: pointer;
  background: ${({ selected }) => (selected ? '#ebebeb' : 'none')};
  text-decoration: none;
  display: ${({ active }) => (active ? 'flex' : 'none')};
  display: flex;

  &:hover {
    background: #ebebeb;
  }

  & ${SidebarItemText} {
    color: ${({ selected }) => (selected ? '#000000' : '#454F5B')} !important;
  }

  & ${SidebarIcon} {
    fill: ${({ selected }) => (selected ? '#000000' : '#454F5B')} !important;
  }
`;

export const SidebarItemText = styled.span`
  margin-left: 16px;
  font-size: 14px;
  font-weight: 500;
  transition: color 0.25s;
  ${SidebarItem}:hover & {
    color: #000000;
  }
`;

const Path = styled.path`
  transition: fill 0.25s;
  ${SidebarItem}:hover & {
    fill: #000000;
  }
`;

export const SidebarIcon = ({ icon }) => {
  switch (icon) {
    case 'voice':
      return (
        <svg
          width="18px"
          height="18px"
          viewBox="0 0 24 24"
          version="1.1"
          xmlns="http://www.w3.org/2000/svg"
          xmlnsXlink="http://www.w3.org/1999/xlink"
        >
          <circle cx="9" cy="9" r="4" />
          <Path
            d="M9 15c-2.67 0-8 1.34-8 4v2h16v-2c0-2.66-5.33-4-8-4zm7.76-9.64l-1.68 1.69c.84 1.18.84 2.71 0 3.89l1.68 1.69c2.02-2.02 2.02-5.07 0-7.27zM20.07 2l-1.63 1.63c2.77 3.02 2.77 7.56 0 10.74L20.07 16c3.9-3.89 3.91-9.95 0-14z"
            id="path-1"
          />
        </svg>
      );
    case 'calendar':
      return (
        <svg
          width="18px"
          height="18px"
          viewBox="0 0 24 24"
          version="1.1"
          xmlns="http://www.w3.org/2000/svg"
          xmlnsXlink="http://www.w3.org/1999/xlink"
        >
          <Path
            d="M17 12h-5v5h5v-5zM16 1v2H8V1H6v2H5c-1.11 0-1.99.9-1.99 2L3 19c0 1.1.89 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2h-1V1h-2zm3 18H5V8h14v11z"
            id="path-1"
          />
        </svg>
      );
  }
};

SidebarIcon.propTypes = {
  icon: PropTypes.string,
};
