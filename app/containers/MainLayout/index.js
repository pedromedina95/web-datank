/**
 *
 * MainLayout
 *
 */

import React, { memo, useState } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import MenuIcon from '@material-ui/icons/Menu';
import Drawer from '@material-ui/core/Drawer';
import { Menu, MenuItem, IconButton } from '@material-ui/core';
import ArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';

import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import { AlignVertical } from 'utils/globalStyledComponents';
import makeSelectMainLayout from './selectors';
import reducer from './reducer';
import saga from './saga';
import {
  MainContainer,
  TopBarContainer,
  Logo,
  Datank,
  Flex,
  LeftMenu,
  Content,
  MobileMenu,
  MenuResponsive,
  H1,
} from './styledComponents';
import { SidebarIcon, SidebarItem, SidebarItemText } from './icons';

const iconStyle = {
  color: '#000000',
  fontSize: 24,
  cursor: 'pointer',
};

export function MainLayout({ children, history }) {
  useInjectReducer({ key: 'mainLayout', reducer });
  useInjectSaga({ key: 'mainLayout', saga });
  const [menuOpen, setMenuOpen] = useState(false);
  const [anchorEl, setAnchorEl] = useState(null);
  const open = Boolean(anchorEl);

  const user = JSON.parse(localStorage.getItem('user'));
  if (!user) history.push('/inicio-sesion');

  const optionSelected = children.props.location.pathname;
  const optionResponsive = (() => {
    switch (optionSelected) {
      case '/':
        return 'Dashboard';
      case '/mis-eventos':
        return 'Mis eventos';
      case '/nueva-charla':
        return 'Quiero dar una charla';
      case '/catalogo-eventos':
        return 'Eventos';
      default:
        return optionSelected.replace('/', '');
    }
  })();

  const handleChangeRoute = route => () => {
    history.push(`${route}`);
    setMenuOpen(false);
  };
  const handleChangeMenuState = () => {
    setMenuOpen(!menuOpen);
  };

  const toggleDrawer = event => {
    if (
      event.type === 'keydown' &&
      (event.key === 'Tab' || event.key === 'Shift')
    ) {
      return;
    }

    setMenuOpen(!menuOpen);
  };

  const handleLogout = () => {
    localStorage.removeItem('token');
    localStorage.removeItem('user');
    history.push('/inicio-sesion');
  };

  const menu = (
    <React.Fragment>
      {user && user.type === 'normal' && (
        <React.Fragment>
          <SidebarItem
            onClick={handleChangeRoute('/eventos')}
            selected={optionSelected === '/eventos'}
          >
            <SidebarIcon icon="calendar" />
            <SidebarItemText>Eventos</SidebarItemText>
          </SidebarItem>
          <SidebarItem
            onClick={handleChangeRoute('/nueva-charla')}
            selected={optionSelected === '/nueva-charla'}
          >
            <SidebarIcon icon="voice" />
            <SidebarItemText>Nueva charla</SidebarItemText>
          </SidebarItem>
        </React.Fragment>
      )}
      {user && user.type !== 'normal' && (
        <SidebarItem
          onClick={handleChangeRoute('/catalogo-eventos')}
          selected={optionSelected === '/catalogo-eventos'}
        >
          <SidebarIcon icon="calendar" />
          <SidebarItemText>Eventos</SidebarItemText>
        </SidebarItem>
      )}
    </React.Fragment>
  );

  return (
    <MainContainer>
      <TopBarContainer>
        <AlignVertical>
          <Logo src="https://pbs.twimg.com/profile_images/902297422226898944/1zYIFQiS_400x400.jpg" />
          <Datank>Datank</Datank>
        </AlignVertical>
        <AlignVertical>
          <IconButton
            aria-label="More"
            aria-owns={open ? 'simple-menu' : undefined}
            aria-haspopup="true"
            onClick={event => setAnchorEl(event.currentTarget)}
          >
            <ArrowDownIcon />
          </IconButton>
          <Menu
            id="simple-menu"
            anchorEl={anchorEl}
            open={open}
            anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
            onClose={() => setAnchorEl(null)}
          >
            <MenuItem key="logout" onClick={handleLogout}>
              Cerrar sesión
            </MenuItem>
          </Menu>
        </AlignVertical>
      </TopBarContainer>
      <MobileMenu>
        <MenuIcon onClick={handleChangeMenuState} style={{ ...iconStyle }} />
        <h1>{optionResponsive}</h1>
        <Drawer open={menuOpen} onClose={toggleDrawer}>
          <MenuResponsive>
            <AlignVertical style={{ marginBottom: 24, paddingLeft: 10 }}>
              <Logo src="https://pbs.twimg.com/profile_images/902297422226898944/1zYIFQiS_400x400.jpg" />
              <Datank>Datank</Datank>
            </AlignVertical>
            {menu}
          </MenuResponsive>
        </Drawer>
      </MobileMenu>
      <Flex>
        <LeftMenu>{menu}</LeftMenu>
        <Content>
          <H1>{optionResponsive}</H1>
          {children}
        </Content>
      </Flex>
    </MainContainer>
  );
}

MainLayout.propTypes = {
  children: PropTypes.element,
  history: PropTypes.object.isRequired,
};

const mapStateToProps = createStructuredSelector({
  mainLayout: makeSelectMainLayout(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(MainLayout);
